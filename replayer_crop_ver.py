import os
import sys
import time
import csv
import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from openpyxl import load_workbook
from PIL import Image, ImageDraw, ImageFont
from matplotlib.backends.backend_qt5agg import \
    FigureCanvasQTAgg as FigureCanvas
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import (QApplication, QFileDialog, QGraphicsPixmapItem,
                             QGraphicsScene)
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import PySpin


from view_morphing.prewarp import find_prewarp
from view_morphing.morph import img_morph
from view_morphing.morph import view_morph

from view_morphing.postwarp import find_postwarp
from view_morphing import utils

def mouse_handler(event, x, y, flags, data):
    if event == cv2.EVENT_LBUTTONDOWN:
        # 標記點位置
        cv2.circle(data['img'], (x,y), 8, (0,0,255), 10, 16) 

        # 改變顯示 window 的內容
        cv2.imshow("img", data['img'])
        
        # 顯示 (x,y) 並儲存到 list中
        print("get points: (x, y) = ({}, {})".format(x, y))
        data['pt'].append((x,y))
        data['already_select']=True
        
class Replayer:
    def __init__(self,camera_num):
        self.camera_num=camera_num
        self.img_pts=[]
        self.select_pts=[]
        self.caps=[]
        self.imgs=[]
        self.imgs_translate=[]
        self.imgs_crop=[]
        self.mtx_r2v=[]
        self.mtx_v2i=[]
        self.distance_translate=[]
        self.distance_crop=[]
        self.boundary=[]
        self.load_pt=True
        self.load_pt_path="Points\marker_position.csv"
        self.already_select=False
        self.finish=False
        for i in range(self.camera_num):
            self.img_pts.append([[0,0],[0,0],[0,0],[0,0]])
            self.select_pts.append([0,0])
        #籃球場禁區
        '''
        #排球場全場
        self.virtual_pt=[[0,0],[18,0],[18,9],[0,9]]
        #籃球場全場
        self.virtual_pt=[[0,0],[28,0],[28,15],[0,15]]
        #籃球場半場
        self.virtual_pt=[[0,0],[14,0],[14,15],[0,15]]
        #籃球場禁區
        self.virtual_pt=[[660,300],[1240,300],[1240,780],[660,780]]
        '''
        self.virtual_pt=[[660,300],[1240,300],[1240,780],[660,780]]
        self.virtual_select_pt=[0,0]
        self.virtual_marker=[0,0]
        self.virtual_center=[0,0]
        #initialize video capture
        self.caps.append(cv2.VideoCapture("Data\C1.mp4"))
        self.caps.append(cv2.VideoCapture("Data\C2.mp4"))
        self.caps.append(cv2.VideoCapture("Data\C3.mp4"))
        self.caps.append(cv2.VideoCapture("Data\C4.mp4"))
        self.caps.append(cv2.VideoCapture("Data\C5.mp4"))
    
    def replay(self):
        for i in range(self.camera_num):
            rst,img=self.caps[i].read()
            #cv2.imwrite("./Result/img"+str(i+1)+".png",img)
            self.imgs.append(img)
            if(not self.load_pt):
                pt=self.get_marker(img)
                
                for j in range(4):
                    self.img_pts[i][j][0]=pt[j][0]
                    self.img_pts[i][j][1]=pt[j][1]
        
        if(not self.load_pt): 
            with open(self.load_pt_path,'w',newline='') as csvfile:
                writer=csv.writer(csvfile)
                for i in range(self.camera_num):
                    for j in range(4):
                        writer.writerow(self.img_pts[i][j])

        if(self.load_pt):
            with open(self.load_pt_path,'r',newline='') as csvfile:
                rows=csv.reader(csvfile)
                for i,row in enumerate(rows):
                    self.img_pts[int(i/4)][i%4][0]=int(row[0])
                    self.img_pts[int(i/4)][i%4][1]=int(row[1])
                    
        #print(img_pts)
        mtx_r2v=self.get_perspective(self.img_pts[2],self.virtual_pt)
        
        while (not self.already_select):
            self.imgs.clear()
            for i in range(self.camera_num):
                rst,img=self.caps[i].read()
                self.imgs.append(img)
        
            self.select_pts[2],self.already_select=self.select_pt(self.imgs[2])
            #print(self.already_select)
        cv2.destroyAllWindows()

        #print(select_pts[2])
        for i in range(2):
            self.virtual_select_pt[i]=(mtx_r2v[i][0]*self.select_pts[2][0]+mtx_r2v[i][1]*self.select_pts[2][1]+mtx_r2v[i][2])/(mtx_r2v[2][0]*self.select_pts[2][0]+mtx_r2v[2][1]*self.select_pts[2][1]+mtx_r2v[2][2])

        '''
        #save virtual image
        img_virtual=cv2.warpPerspective(imgs[2],mtx_r2v,(imgs[2].shape[1],imgs[2].shape[0]))
        for  j in range(4):
            for i in range(2):
                virtual_marker[i]=round((mtx_r2v[i][0]*img_pts[2][j][0]+mtx_r2v[i][1]*img_pts[2][j][1]+mtx_r2v[i][2])/(mtx_r2v[2][0]*img_pts[2][j][0]+mtx_r2v[2][1]*img_pts[2][j][1]+mtx_r2v[2][2]))

                virtual_center[i]+=virtual_marker[i]
            
            cv2.circle(img_virtual,(virtual_marker[0],virtual_marker[1]),8,(0,0,255), 10, 16)
            cv2.imshow("virtual",img_virtual)
            cv2.waitKey(0)
            
        cv2.circle(img_virtual,(int(virtual_center[0]/4),int(virtual_center[1]/4)),8,(0,255,255), 10, 16)
        cv2.imshow("virtual",img_virtual)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite("./Result/virtual.png",img_virtual)'''
        #print(virtual_select_pt)
        for i in range(self.camera_num):
            self.mtx_v2i.append(self.get_perspective(self.virtual_pt,self.img_pts[i]))
            
        for i in range(self.camera_num):
            for j in range(2):
                self.select_pts[i][j]=round((self.mtx_v2i[i][j][0]*self.virtual_select_pt[0]+self.mtx_v2i[i][j][1]*self.virtual_select_pt[1]+self.mtx_v2i[i][j][2])/(self.mtx_v2i[i][2][0]*self.virtual_select_pt[0]+self.mtx_v2i[i][2][1]*self.virtual_select_pt[1]+self.mtx_v2i[i][2][2]))
            
            
        #print(select_pts)
        #print(imgs[0].shape)

        for i in range(self.camera_num):
            self.imgs_translate.append(self.translate_pt2center(self.imgs[i],self.select_pts[i],self.distance_translate))
            '''cv2.imshow("translate",imgs_translate[i])
            cv2.imwrite("./Result/img_translate_"+str(i+1)+".png",imgs_translate[i])
            cv2.waitKey(0)
            cv2.destroyAllWindows()'''

        boundary=self.find_boundary(self.imgs,self.distance_translate,self.camera_num)
        #print(boundary)

        #print(distance_translate)
        for i in range(self.camera_num):
            self.imgs_crop.append(self.img_crop(self.imgs_translate[i],boundary))
            '''cv2.imshow("crop",imgs_crop[i])
            cv2.imwrite("./Result/img_crop_"+str(i+1)+".png",imgs_crop[i])
            cv2.waitKey(0)
            cv2.destroyAllWindows()'''

        while(not self.finish):
            for i in range(self.camera_num):
                if (not self.caps[i].isOpened()):
                    self.finish=True
                    break
            if (self.finish):
                break
            
            self.imgs_crop.clear()
            for i in range(self.camera_num):
                ret,img=self.caps[i].read()
                if img is None:
                    self.finish=True
                    break
                img_t=self.translate_pt2center(img,self.select_pts[i],self.distance_translate)
                img_c=self.img_crop(img_t,boundary)
                self.imgs_crop.append(img_c)
                cv2.imshow("C"+str(i+1),img_c)
                
            if (cv2.waitKey(1)==ord('q')):
                break
        cv2.destroyAllWindows()
                    
    def select_pt(self,img):
        data={}
        data['img']=img.copy()
        data['pt']=[]
        data['already_select']=False
        point=[]
        
        cv2.imshow("img",img)
        cv2.setMouseCallback("img",mouse_handler,data)
        
        if cv2.waitKey(1)==ord('q'):
            cv2.destroyAllWindows()
        
        already_select=data['already_select']
        #print(already_select)
        
        if not already_select:
            return [0,0],already_select
        point.clear()
        point.append(data['pt'][0][0])
        point.append(data['pt'][0][1])
        
        #points.append(point.copy())
            
        return point.copy(),already_select
        
    def get_marker(self,img):
        data={}
        data['img']=img.copy()
        data['pt']=[]
        data['already_select']=False
        point=[]
        points=[]
        select_area_center=[0,0]
        
        cv2.imshow("img",img)
        for j in range(4):
            cv2.setMouseCallback("img",mouse_handler,data)
        
        #cv2.imwrite("./Result/img_marker_"+str(j+1)+".png",data['img'])
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
        for j in range(4):
            point.clear()
            point.append(data['pt'][j][0])
            point.append(data['pt'][j][1])
            
            select_area_center[0]+=data['pt'][j][0]
            select_area_center[1]+=data['pt'][j][1]
            
            points.append(point.copy())
            
        cv2.circle(data['img'],(int(select_area_center[0]/4),int(select_area_center[1]/4)),8,(0,255,255), 10, 16)
        cv2.imshow("img", data['img'])
        cv2.waitKey(0)
        cv2.destroyAllWindows()
            
        return points
    
    def get_perspective(self,src,dst):
    
        pts_1=np.float32(src)
        pts_2=np.float32(dst)
        #print(pts_1)
        #print(pts_2)
        mtx=cv2.getPerspectiveTransform(pts_1,pts_2)
        #print(mtx)
        
        return mtx
    
    def translate_pt2center(self,img,pt,distance):
        h_img=img.shape[0]
        w_img=img.shape[1]
        pt_x=pt[0]
        pt_y=pt[1]
        
        distance_x=w_img/2-pt_x
        distance_y=h_img/2-pt_y
        
        T=np.float32([[1,0,distance_x],[0,1,distance_y]])
        rst=cv2.warpAffine(img,T,(w_img,h_img))
        distance.append([distance_x,distance_y])
        
        return rst
    
    def find_boundary(self,imgs,distance,camera_num):
        left=0
        right=imgs[0].shape[1]
        top=0
        buttom=imgs[0].shape[0]
        
        for i in range(camera_num):
            #print([[left,top],[right,buttom]])
            if(distance[i][0]>left):
                left=distance[i][0]
                right=imgs[0].shape[1]-left
            if (distance[i][0]<0 and imgs[i].shape[1]+distance[i][0]<right):
                right=imgs[i].shape[1]+distance[i][0]
                left=imgs[0].shape[1]-right
            if(distance[i][1]>top):
                top=distance[i][1]
                buttom=imgs[0].shape[0]-top
            if(distance[i][1]<0 and imgs[i].shape[0]+distance[i][1]<buttom):
                buttom=imgs[i].shape[0]+distance[i][1]
                top=imgs[0].shape[0]-buttom
        
        return [[round(left),round(top)],[round(right),round(buttom)]]
    
    def img_crop(self,img,boundary):
        #print(boundary)
        rst=img[boundary[0][1]:boundary[1][1],boundary[0][0]:boundary[1][0]]
            
        return rst
    
replayer=Replayer(5)
replayer.replay()